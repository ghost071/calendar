package model

import (
	"sync/atomic"
	"time"
)

var counter int64

type Event struct {
	ID          int64         `json:"id"`
	Title       string        `json:"name"`
	Description string        `json:"description"`
	Begin       time.Time     `json:"begin"`
	Duration    time.Duration `json:"end"`
	index       int
}

func NewEvent(title, description string, start time.Time, dur time.Duration) *Event {
	id := counter
	atomic.AddInt64(&counter, 1)
	return &Event{
		ID:          id,
		Title:       title,
		Description: description,
		Begin:       start,
		Duration:    dur,
	}
}

func (e *Event) SetIndex(i int) {
	e.index = i
}

func (e *Event) GetIndex() int {
	return e.index
}

func (e *Event) SetTime(t time.Time) {
	e.Begin = t
}

func (e *Event) SetDuration(d time.Duration) {
	e.Duration = d
}

func (e *Event) SetTitle(s string) {
	e.Title = s
}

func (e *Event) SetDescription(s string) {
	e.Description = s
}
