package store

import "app/internal/model"

type Storage interface {
	GetById(string, int64) (*model.Event, error)
	AddEvent(string, *model.Event)
	DeleteEvent(string, int64) (*model.Event, error)
	UpdateEvent(string, int64, *model.Event) (*model.Event, error)
}
