package config

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

type Cfg struct {
	Port string
	Host string
}

func (c *Cfg) GetAddr() string {
	return fmt.Sprintf("%s:%s", c.Host, c.Port)
}

func LoadConfig() *Cfg {
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}
	return &Cfg{
		Port: os.Getenv("APP_PORT"),
		Host: os.Getenv("APP_HOST"),
	}
}
