package handler

import (
	"app/internal/apperr"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const (
	DayPath   string = "/events_for_day"
	WeekPath         = "/events_for_week"
	MonthPath        = "/events_for_month"
	Fail             = "FAIL"
)

var status = map[string]string{
	"/create_event": "CREATED",
	"/update_event": "UPDATED",
	"/delete_event": "DELETED",
}

type ResponseData struct {
	Result interface{} `json:"Result"`
	Msg    string      `json:"Status"`
	Error  string      `json:"Error"`
}

func toJSON(result interface{}, infoMsg string, err error) ([]byte, error) {
	var errMsg string
	if err != nil {
		errMsg = err.Error()
	}
	r := &ResponseData{Result: result, Msg: infoMsg, Error: errMsg}
	bytes, err1 := json.Marshal(r)
	if err1 != nil {
		fmt.Println(err)
		return nil, err
	}
	return bytes, nil
}

func readBody(data io.ReadCloser) (string, error) {
	buff := bytes.NewBuffer([]byte{})
	if _, err := io.Copy(buff, data); err != nil {
		return "", err
	}

	return buff.String(), nil
}

func switchStatus(err error) int {
	if err == nil {
		return http.StatusOK
	}

	switch err.(type) {
	case *apperr.ServiceError:
		return http.StatusServiceUnavailable
	case *apperr.ValidateError:
		return http.StatusBadRequest
	case *apperr.NoContentError:
		return http.StatusNoContent
	default:
		return http.StatusInternalServerError
	}
}

func setStatus(w http.ResponseWriter, r *http.Request, arg int) {
	w.WriteHeader(arg)
	if v, ok := r.Context().Value("status").(*int); ok {
		*v = arg
	}
}

func writeResp(res interface{}, err error, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	success := status[r.URL.Path]
	fail := Fail

	if err != nil {
		data, jsonErr := toJSON(res, fail, err)
		if jsonErr != nil {
			setStatus(w, r, switchStatus(jsonErr))
			log.Println(err)
			return
		}
		setStatus(w, r, switchStatus(err))
		w.Write(data)
		return
	}
	data, err := toJSON(res, success, err)
	if err != nil {
		setStatus(w, r, switchStatus(err))
		log.Println(err)
	}

	setStatus(w, r, switchStatus(err))
	w.Write(data)
}
