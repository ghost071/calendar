package handler

import (
	"app/internal/app/service"
	"app/internal/app/store"
	"app/internal/model"
	"net/http"
)

const (
	Day   int = 1
	Week      = 7
	Month     = 30
)

type Handler struct {
	Storage *store.Cache
	Router  http.Handler
}

func NewHandler() *Handler {
	return &Handler{
		Storage: store.NewCache(),
	}
}

func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	params, err := readBody(r.Body)
	r.Body.Close()
	if err != nil {
		setStatus(w, r, switchStatus(err))
		return
	}

	res, err := service.CreateEvent(h.Storage, params)
	writeResp(res, err, w, r)
}

func (h *Handler) Update(w http.ResponseWriter, r *http.Request) {
	params, err := readBody(r.Body)
	r.Body.Close()
	if err != nil {
		setStatus(w, r, switchStatus(err))
		return
	}

	res, err := service.UpdateEvent(h.Storage, params)
	writeResp(res, err, w, r)
}

func (h *Handler) Delete(w http.ResponseWriter, r *http.Request) {
	params, err := readBody(r.Body)
	r.Body.Close()
	if err != nil {
		setStatus(w, r, switchStatus(err))
		return
	}

	res, err := service.DeleteEvent(h.Storage, params)
	writeResp(res, err, w, r)
}

func (h *Handler) EventsForPeriod(w http.ResponseWriter, r *http.Request) {
	params := r.URL.RawQuery

	var (
		res []*model.Event
		err error
	)

	switch r.URL.Path {
	case DayPath:
		res, err = service.EventsForPeriod(h.Storage, params, Day)
	case WeekPath:
		res, err = service.EventsForPeriod(h.Storage, params, Week)
	case MonthPath:
		res, err = service.EventsForPeriod(h.Storage, params, Month)
	}

	writeResp(res, err, w, r)
}
