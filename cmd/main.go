package main

import (
	"app/internal/app/apiserver"
	"app/internal/app/config"
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg := config.LoadConfig()
	server := apiserver.NewServer(cfg)

	ctx, cancel := context.WithCancel(context.Background())
	go sigHandler(server, cancel)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()
	log.Printf("Server is starting on %s\n", cfg.GetAddr())
	<-ctx.Done()
}

func sigHandler(srv *http.Server, cancel context.CancelFunc) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGABRT,
	)
	<-sig
	srv.Shutdown(context.TODO())
	cancel()
}
