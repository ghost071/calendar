### Api
#### POST
* /create_event
* /delete_event
* /update_event

#### GET
* /events_for_day
* /events_for_week
* /events_for_month

### Параметры
* Post - передаются в теле запроса x-www-form-urlencoded
* Get - querystring

#### Список параметров
* begin - время начала события. Задается в формате mm.dd.yy или mm.dd.yy-h:m
* title - наименование
* duration - продолжительность (опционально)
* description - описание (опционально)

* date - дата события в формате mm.dd.yy (обязательный параметр для update/delete)
* id - id события (обязательный параметр для update/delete)

### Логгирование запросов
![](./screens/img3.png)

### Примеры ответов
![](./screens/img1.png)
![](./screens/img2.png)
